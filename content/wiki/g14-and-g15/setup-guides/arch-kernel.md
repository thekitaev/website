+++
title = "Arch linux - Kernel"
description = "G14/15 Arch linux kernel setup guide"
sort_by = "none"
template = "page/wiki.html"
author = "Mateusz Schyboll"
+++

## Setup

Please see [linux-g14](https://aur.archlinux.org/packages/linux-g14/)
note, it is preferred to download the package directly and build it locally for example:

```bash
pikaur -G linux-g14
cd linux-g14
makepkg -i -s --skippgpcheck
```

Some aur managers skip installing the linux-g14-headers.

For Nvidia driver please see [nvidia-dkms](https://www.archlinux.org/packages/extra/x86_64/nvidia-dkms/), don't use the regular nvidia module, it is only for the stock kernel.
After installing the kernel you need to setup you bootloader, grub should automatic detect the new kernel when doing config updates `grub-mkconfig -o /boot/grub/grub.cfg`
For systemd-boot create an entry like that for example:

```text
title Arch Linux
linux /vmlinuz-linux-g14
initrd /amd-ucode.img
initrd /initramfs-linux-g14.img
options root=/dev/disk/by-uuid/e621fdf1-5ef1-4b5b-805c-f1aa381cdaf1 rw audit=0
```

`initrd /amd-ucode.img` is optional for updating the cpu mircocode, you need for that that package [amd-ucode](https://www.archlinux.org/packages/core/any/amd-ucode/)
