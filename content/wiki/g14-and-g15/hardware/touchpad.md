+++
title = "Touchpad - ITE Tech. Inc. ITE Device(8910)"
description = "Howto setup the touchpad"
sort_by = "none"
template = "page/wiki.html"
author = "Armas Spann"
+++

* Driver: ELAN1201
* VEN:DEV: 04F3:3098

Kernel configuration:

```bash
Processor type and features  --->
    [*] AMD ACPI2Platform devices support
Device Drivers  --->
    -*- Pin controllers  --->
        <*> AMD GPIO pin control
    HID support  --->
        Special HID drivers  --->
            <*> HID Multitouch panels
        I2C HID support  --->
            <*> HID over I2C transport layer
    I2C support  --->
        I2C Hardware Bus support  --->
            <*> AMD MP2 PCIe
            <*> Synopsys DesignWare Platform
            <*> Synopsys DesignWare PCI
    Input device support  --->
        Mice  --->
            <*> ELAN I2C Touchpad support
            [*] Enable I2C support
            [*] Enable SMbus support
```

/etc/X11/xorg.conf.d/99-synaptics-overrides.conf:

```bash
Section  "InputClass"
    Identifier "touchpad overrides"
    MatchDriver "synaptics"
    MatchIsTouchpad "on"
    Option  "TapButton1"  "1"
    Option  "TapButton2" "3"
    Option  "VertEdgeScroll"  "1"
    Option  "RBCornerButton" "3"
EndSection
```

**Alternate setup using libinput (thanks to ZTube):**

/etc/X11/xorg.conf.d/30-touchpad.conf:

```bash
Section "InputClass"
    Identifier "touchpad"
    Driver "libinput"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
    Option "TappingButtonMap" "lrm"
    Option "NaturalScrolling" "true"
EndSection
```
