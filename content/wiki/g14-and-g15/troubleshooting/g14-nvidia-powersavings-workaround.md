+++
title = "Asus Zephyrus G14 GPU powersaving"
description = "Troubleshoot GPU powersavings by using ACPI and bbswitch"
sort_by = "none"
template = "page/wiki.html"
author = "Cole Deck"
+++

In order to get the best battery life, the dedicated GPU must be disabled.

## With BIOS version 216 (Most G14 models) / 208 (Some G14 and G15 models) or newer

It is recommended to upgrade to version 216, 217 etc. for the easiest and best experience for powering off the gpu. On other models the "good" version might be called 208 and 209. Just whatever the latest is what you want. If you are on an older version (212 on the G14) then you will have a lot of trouble turning off the GPU, so we recommend you upgrade. You can update the BIOS without Windows by downloading the EZ flash version of the update from Asus, and then putting it on a FAT32 usb drive or just your boot partition. Boot into the bios and go to EZ flash, and find the file to update.

On BIOS 216 and newer, set the power control of the gpu to auto, you can do it manually with `echo auto | sudo tee /sys/bus/pci/devices/0000:01:00.0/power/control`

You can check the status of the GPU power state by doing `cat /sys/bus/pci/devices/0000:01:00.0/power/runtime_status`. If it says `suspended` then the GPU is powered off.
As long as the GPU is not in use (nvidia driver not loaded) then it will turn off.

Currently runtime D3 is still not working, so you can't be in hybrid mode and have the GPU off. 

In order to automate powering off the GPU, you can use optimus-manager or asus-nb-ctrl (recommended).

`asus-nb-ctrl` will control the pci power automatically. For best experience with asus laptops, and GPU power management, consider using fedora. On arch the issues with asus-nb-ctrl graphics switching are fixed now. Just make sure you don't have *any* .conf files that define anything related to GPU configurations (touchpad and display options are fine) in /etc/X11/xorg.conf.d as that will most likely override asus-nb-ctrl's setting. Here's a simple how to use:

Make sure in asusd.conf that `"gfx_managed": true` (it is usually true by default)

Then use the command `asusctl graphics` to switch and see status. For best battery, switch to integrated, e.g. `asusctl graphics -m integrated`. On integrated mode asusctl will also call PCI remove on the GPU, so it can't be accidentally woken up for any reason.


If using optimus-manager instead, set the config to 
```
[optimus]
switching=none
pci_power_control=yes
pci_remove=yes
pci_reset=no
```

---
## BIOS version 212 and older

This is preserved for archival purposes, but there is no reason not to update to a newer BIOS version.**

### BBSwitch

Thanks to /u/FailSpai on reddit (FailSpy#0001 on Discord), we know the ACPI call to toggle the GPU. The GPU can be powered off and on with the call "\\_SB.PCI0.GPP0.PG00". He's made it easier than calling that manually - FailSpy made a patch for `bbswitch` in order to have it manage the GPU instead of you. His patch just makes the ACPI calls when it's safe, and it turns the GPU on before suspend to prevent issues.

On Arch you can install the AUR package `bbswitch-g14-dkms-git`. For other distros, there are installation instructions [here](https://github.com/FailSpy/rog-zephyrus-gpu-patches/tree/master/bbswitch/0.8). 

To load the module, use `sudo modprobe bbswitch`. Once loaded, you can turn the GPU on and off with `sudo tee /proc/acpi/bbswitch<<<ON` and `sudo tee /proc/acpi/bbswitch<<<OFF` respectively.

To check whether the GPU is on or off, run `sudo cat /proc/acpi/bbswitch`. And to see the current powerdraw in microWatts, run `cat /sys/class/power_supply/BAT0/power_now`. For example a value of 6500000 is 6.5 watts.

In order to load the module at boot and turn off the GPU automatically, put the line `options bbswitch load_state=0 unload_state=1` in a new file, `/etc/modprobe.d/bbswitch.conf`. Note that `load_state=0` prohibits the dGPU from being utilized at all.

In order to actually use the GPU, there are 2 main methods: optimus-manager, and bumblebee.
They both have their advantages and disadvantages. Optimus-manager allows for easy switching between various modes, such as nvidia by default (almost no power management, for external displays), reverse prime (iGPU as default, partial power management, only apps run with `prime-run` will use the dGPU), and iGPU only (dGPU completely off). However, switching between these configurations requires restarting your X session and closing all open programs. Bumblebee allows for using the GPU and turning it on and off without restarting X, at the cost of ~3% performance and has many compatibility issues with Steam, vulkan, and more.

### Optimus Manager (Recommended)

[`optimus-manager-amd`](https://github.com/Kr1ss-XD/optimus-manager#branch=amdigpu-compat) (available on the AUR as `optimus-manager-amd-git`) is needed, as the regular optimus-manager does not work with AMD iGPUs. `optimus-manager-amd` will automatically handle switching Xorg sessions and automatically depowering the GPU when it's not used. When in `amd` mode, the laptop only draws 5.5-6.5w on idle. At minimum brightness, it could get as low as 4.8 watts with nothing running. 

Simply set the switching mode to `bbswitch` in `/etc/optimus-manager/optimus-manager.conf`:

```
[optimus]
switching=bbswitch
pci_power_control=no
pci_remove=no
pci_reset=no
```

Then you can use `optimus-manager` to switch between modes and set a default mode.

If specifying the `bbswitch` kernel parameters from above, ensure that `load_state` is set to `1`, e.g. `options bbswitch load_state=1`.

### Bumblebee

For people who need the GPU to work spontaneously without an X restart, and also get full powersaving, then Bumblebee could be useful. If you encounter issues with Steam, Proton, Vulkan, etc., it is most likely because of the way Bumblebee works and often simply can't be fixed. Bumblebee is no longer actively developed. If you run into issues, do not ask the asus-linux team for help, we don't know how to fix it. We will simply tell you to use optimus-manager instead.

Nevertheless, install `bumblebee-picasso-git` from the AUR, or get the patches and build separately [here](https://github.com/FailSpy/rog-zephyrus-gpu-patches/tree/master/bumblebee/3.2). In `/etc/bumblebee/bumblebee.conf`, set `Driver=nvidia` and `PMMethod=bbswitch`. For Arch, see the comments on the AUR package page for a patch to allow building with GCC 10. You may also need to add `alias nvidia_modeset off` in `/etc/modprobe.d/nvidia.conf` if Bumblebee errors about being unable to unload the nvidia module. Then you can use `primusrun` to run applications on the GPU.

## Manual ACPI Calls

This is the manual method for controlling the GPU, and is no longer necessary.

You the `acpi_call` kernel module, on Arch this can be installed with the `acpi_call` or `acpi_call-dkms` package.

To turn off the GPU, run `sudo sh -c 'echo "\\_SB.PCI0.GPP0.PG00._OFF" > /proc/acpi/call'`. Similarly, to turn it back on, `sudo sh -c 'echo "\\_SB.PCI0.GPP0.PG00._ON" > /proc/acpi/call'`. You can verify the state of the GPU by running `sudo sh -c 'echo "\\_SB.PCI0.GPP0.PEGP.SGST" > /proc/acpi/call'`, then running `sudo cat /proc/acpi/call`. 0x0 means off, and anything above 0 means on. Alternatively, just look at the total system power draw (must be unplugged from AC to get a number), `cat /sys/class/power_supply/BAT0/power_now` and if it's under 10-11w then it's off.

Note that you can really only power the GPU off when its not in use. For example, powering it off with a render offload setup will leave the GPU unusable until X is restarted, and may cause issues during shutdown/reboot. You must be in an X session that does not touch the GPU at all. 

## Other powersaving tips

The above power management allows for the same, or better, idle power draw than Windows, as long as you have done other tweaks (disable boost, change CPU governor, etc). Make sure to install and enable TLP, a big help is lowering the maximum clockspeed of the CPU to 1.7Ghz, and using the powersave governor, while on battery. `powertop --auto-tune` can help too.

To enable video acceleration in Firefox, you'll need Firefox version 80+, see [this Arch Wiki page](https://wiki.archlinux.org/index.php/Firefox#Hardware_video_acceleration). This will save around 2-3w of power on YouTube for example.


---


Again, thanks to /u/FailSpai for digging through the ACPI tables to find the right call, and implementing it into `bbswitch`. You can find his original comment about the ACPI calls [here](https://www.reddit.com/r/VFIO/comments/hx5j8q/success_with_laptop_gpu_passthrough_on_asus_rog/g0m3kvh/), and his github repo with various patches and configurations for GPU powersaving at [rog-zephyrus-gpu-patches](https://github.com/FailSpy/rog-zephyrus-gpu-patches). If you need help configuring, feel free to ask for help in the [ROG for Linux discord server](https://discord.gg/JcXaNew).
