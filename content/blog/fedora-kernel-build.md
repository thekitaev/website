+++
title = "Build a fedora kernel"
description = "Patches you might require won't always be backported - this post will help you build a kernel with the required patches"
date = 2020-08-07
template = "page/default.html"
draft = false
+++

## Fedora Kernel Build

The primary source of information on building a custom kernel in fedora is [here](https://fedoraproject.org/wiki/Building_a_custom_kernel#Building_a_Kernel_from_the_Fedora_source_tree). This post will step you through that with an additional step to add the patches you may need.

### Get the source

First, where-ever you feel like, clone the kernel packaging source, for example:

```bash
cd ~ && mkdir projects && cd ~/projects
fedpkg clone -a kernel
```

Then cd in to the new directory and checkout the required branch matching the fedora release number you need:

```bash
cd ~/projects/kernel
git checkout origin/f32
```

### Prepare for build

In `kernel.spec` is where you will do most of the work, so open up that file in your
favourite text editor which is vim and search for this line:

```bash
# define buildid .local
```

and replace it with:

```bash
%define buildid .local
```

then a few hundred or so lines further down the file you will find lines like this one:

```bash
Patch126: 0001-Work-around-for-gcc-bug-https-gcc.gnu.org-bugzilla-s.patch
```

this is where you will need to add patches, an example is adding the `asus-hid` driver
patch required for ASUS laptops with the 0x1866 device (N-Key Keyboard). So below the
line above add:

```bash
# ROG Laptops
Patch127: 0001-HID-asus-add-support-for-ASUS-N-Key-keyboard-v5.7.patch
```

and lastly, grab the patch - it must be in the same directory as `kernel.spec`, run:

```bash
cd ~/projects/kernel
wget https://gitlab.com/asus-linux/asus-nb-ctrl/-/raw/next/kernel-patch/0001-HID-asus-add-support-for-ASUS-N-Key-keyboard-v5.7.patch
```

and then build using `fedpkg local`.

### Install

All the rpm packages will be located in `~/projects/kernel/x86_64/`, it's likely that
you will need to install all except the debug packages.

### Updating

Follow the directions [here](https://fedoraproject.org/wiki/Building_a_custom_kernel#Building_a_Kernel_from_the_Fedora_source_tree).
